## Programming Language Project Ideas So Far

#### Racket Syntax Converter

- Defines rules to transform other syntax (e.g. Java-style) to Racket syntax
- Example: Might transform `main(String[] args): int {...}` into `'(def-func main (input (args (Array String))) (output int) (body ...))`
- Goal: To allow people to write DSLs in Racket which don't use Racket syntax, or take existing DSLs and give them non-Racket syntax.

#### Racket Docs

- Simulate JavaDocs' usage of comment braces (/* */) that let you auto-generate JavaDoc pages that include signatures and purpose statements
- Goal: To use special comments in Racket devoted to signatures and purpose statement, with options to generate a webpage 

#### Parallel Racket

- Prevent all side-effects or encode them (e.g. in `Monad`s and linear-typed `World` transformations).
- Can evaluate expressions in parallel, at least in theory.