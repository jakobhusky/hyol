; Problem Set 5
; Hain, Jakob
; Zhang, Kevin

Basic
-----
(maze (a b) (c d) (e f) ... entrances: a b c ... exits: d e f ...)
Creates a maze with
- An edge from a to b, an edge from c to d, an edge from e to f, ...
- The entrances a, b, c, ...
- The exits d, e, f, ...
and sets it to the current maze.

(path)
Outputs a path from one of the maze's entrances to one of it's exits.

(path?)
Outputs whether or not a path exists from one of the current maze's entrances to one of it's exits.

(path to a)
Outputs a path from one of the current maze's entrances to the node a. Outputs #false if no path exists.

(path? to a)
Outputs whether or not a path exists from one of the current maze's entrances to the node a.

(path from a)
Outputs a path from the node a to one of the current maze's exits. Outputs #false if no path exists.

(path? from a)
Outputs whether or not a path exists from the node a to one of the current maze's exits.

(path from a to b)
Outputs a path from the node a to the node b within the current maze.

(path? from a to b)
Outputs whether or not a path exists the node a to the node b within the current maze.

Edge cases
----------
The paths returned contain the entrances and exits, e.g.

(maze (a b) (b c) entrances: a exits: c)
(path)

will output '(a b c).

The "current maze" refers to the last maze created. So in

(maze (a b) entrances: a exits: b)
(path?)
(maze (a b) (b c) entrances: a exits: c)
(path-to d)
(path-from d)

The path? function will try to find a path in the first maze, and the path-to and path-from functions will try to find a path in the second maze.

If a path function is used before a current maze is created, like in

(path?)
(maze (a b) entrances: a exits: b)

the program will fail.

Mazes can be created which don't contain their entrances or exits, e.g.

(maze (a b) entrances: a exits: c)

These entrances and exits can usually be ignored - the program will never find a path using the entrances or exits. For example,

(maze (a b) entrances: a exits: c)
(path)

will return #false.

But there's 1 exception - if one of the entrances is equivalent to one of the exits, a path can be found using that entrance and exit, even if it isn't in the maze. For example,

(maze (a b) entrances: c exits: c)
(path)

will return '(c).
